import React, { useRef, useState } from "react";
import "./home.scss";

function Home() {
  const myElementRef = useRef<any>(null);

  const [wizardXPosition, setWizardXPosition] = useState(0);
  const [wizardYPosition, setWizardYPosition] = useState(0);
  const [wizardFacingLeft, setWizardFacingLeft] = useState(false);

  const handleClick = (e: any) => {
    const x = e.clientX;
    const y = e.clientY;

    const rect = e.currentTarget.getBoundingClientRect();
    const offsetX = x - rect.left;
    const offsetY = y - rect.top;

    const wizardWidth = myElementRef.current.offsetWidth;
    const wizardHeight = myElementRef.current.offsetHeight;

    const wizardCenterX = rect.left + rect.width / 2;

    if (x < wizardCenterX) {
      setWizardFacingLeft(false);
    } else {
      setWizardFacingLeft(true);
    }

    console.log("rectNew", offsetX);
    setWizardXPosition(offsetX - wizardWidth / 2 - rect.width / 2);
    setWizardYPosition(offsetY - wizardHeight / 2);
  };

  console.log("wizardXPosition", wizardXPosition, wizardYPosition);

  const wizardImg =
    "https://www.wizard.financial/static/media/wizaart-img.56787174.gif";

  return (
    <div
      className="home-container w-100 vh-100 position-relative"
      onClick={handleClick}
    >
      <img
        ref={myElementRef}
        src={wizardImg}
        alt=""
        style={{
          position: "absolute",
          //   top: `${wizardYPosition}`,
          //   left: `${wizardXPosition}`,
          transform: `translate(${wizardXPosition}px, ${wizardYPosition}px) scaleX(${
            wizardFacingLeft ? -1 : 1
          }`,
          transition: "all 1.2s ease-in-out",
        }}
      />
    </div>
  );
}

export default Home;
